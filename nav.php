<div class="fha w" id=menu>
	<a href=images.php>
		Photos
	</a>
	<a href=contacts.php>
		Contacts
	</a>
	<a href=dons.php>
		Dons
	</a>
	<a href=petition.php>
		Pétitions
	</a>
</div>
<style>
#menu
{
	background:rgba(0,0,0,.5);
}
#menu>a
{
	transition:.3s;
	display:flex;
	flex-direction:column;
	justify-content:center;
	font-size:4vw;
	margin:1vw;
	text-align:center;
	border-radius:3vw;
	padding:1vh;
	background:white;
	color:rgba(0,0,0,1);
}
</style>
