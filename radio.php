<head>
<title>Direct</title>
<?php include "header.php";?>
<?php include "views_count.php";?>
<?php include "fb_link.php";?>
</head>
<div class="fhb w">
<?php
	system('
		for i in $(ls -t Videos);
		do
			if [ -f "Videos/$i/video.mp4" ];
			then
				echo "<a href=\"Videos/$i/video.mp4\">";
				echo "<div class=clip>";
				echo "<video class=video_live src=\"Videos/$i/video.mp4\" controls></video>";

				echo "<div class=titre><u>";
				cat Videos/$i/titre.txt;
				echo "</u></div>";

				echo "<div class=auteur>";
				cat Videos/$i/auteur.txt;
				echo "</div>";
				echo "<div class=publication>";
				cat Videos/$i/date.txt;
				echo "</div>";
				echo "<div class=description>";
				cat Videos/$i/description.txt;
				echo "</div>";
				echo "</div></a><br>";
			elif [ -f "Videos/$i/video.webm" ];
			then
				echo "<a href=\"Videos/$i/video.webm\">";
				echo "<div class=clip>";
				echo "<video class=video_live src=\"Videos/$i/video.webm\" controls></video>";

				echo "<div class=titre><u>";
				cat Videos/$i/titre.txt;
				echo "</u></div>";

				echo "<div class=auteur>";
				cat Videos/$i/auteur.txt;
				echo "</div>";
				echo "<div class=publication>";
				cat Videos/$i/date.txt;
				echo "</div>";
				echo "<div class=description>";
				cat Videos/$i/description.txt;
				echo "</div>";
				echo "</div></a><br>";
			elif [ -f "Videos/$i/video.php" ];
			then
				echo "<a href=Videos/$i/video.php>";
				echo "<div class=clip>";
				echo "<div class=fva>";
				cat Videos/$i/video.php;
				echo "</div>";
				echo "</div></a><br>";
			fi
		done;
	');
?>
</div>
<style>
.titre
{
	font-size:5vw;
}
.auteur
{
	font-size:3vw;
}
.publication
{
	font-size:2vw;
}
.description
{
	font-size:3vw;
}

.description
{
	padding:1vw;
}
.video_live
{
	width:25vw;
}
.clip
{
	text-align:center;
	width:100%;
	padding:1vw;
	margin:3vh;
	border-radius:1vw;
	color:black;
	font-size:4vw;
	text-shadow:-1px 1px 0 black;
	border:solid 2px black;
	background:rgba(255,255,128,1);
}
</style>
<script>
	document.documentElement.scrollTop = 0;
	document.getElementById('spacer').style.height=document.getElementById('header').clientHeight + "px";
</script>
