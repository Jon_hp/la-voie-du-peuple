<div id=notre_mission class="text-justify vdp">	
		<h2>
			Mission
		</h2>
Notre mission est humanitaire et consiste à aider les victimes de la crise socio-économique. Trouver un endroit d'hébergement à faible coût aux familles sans logis. Ouverture d'un centre de dépôt bientôt pour dons alimentaires, vestimentaires et mobiliers de toutes sortes. L'organisme a dix chambres pour les bénévoles qui veulent rester sur les lieux. Présentement en création d'une radio-direct pour que tous nos frères et soeurs puissent s'affirmer librement. Merci à tous de votre collaboration! Au plaisir de vous rencontrer.
</div>	
