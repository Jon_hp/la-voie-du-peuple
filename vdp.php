<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css2?family=Miltonian+Tattoo&display=swap" rel="stylesheet">
<style>
h2
{
	text-align:center;
}
a
{
	text-decoration:none;
}
iframe
{
	border:solid 2px #158F9D;
}
.streaming
{
	width:100vw;
}
.text-justify
{
	text-align:justify;
}
.text-center
{
	text-align:center;
}
.vdp
{
	width:83vw;
	padding:5vw;
	margin:0 auto;
	border-radius:1vw;
	color:black;
	font-size:4vw;
	text-shadow:-1px 1px 0 black;
	border:solid 2px black;
	background:rgba(255,255,128,1);
	transition:.4s;
}

.logo
{
	font-size:2vh;
	text-shadow:0px 0px 1px black;
	color:white;
	background:rgba(0, 0, 0,.7);
	transition:.4s;
}
.logo:hover
{
	color:rgba(181, 231, 245);
	text-shadow:0px 0px 1px black,0 0 7px black;
}
:root
{
	background:#40b1ef;
	background-size:cover;
	font-family: 'Miltonian Tattoo', cursive;
	padding:0;
	background-position:fixed;
}

.f1{flex:1;}
.f2{flex:2;}
.f3{flex:3;}
.f4{flex:4;}
.f5{flex:5;}
.f6{flex:6;}
.f7{flex:7;}
.f8{flex:8;}
.f9{flex:9;}
.w
{
	flex-wrap:wrap;
}
.fhc
{
	display:flex;
	justify-content:center;
}
.fhs
{
	display:flex;
	justify-content:flex-start;
}
.fhe
{
	display:flex;
	justify-content:flex-end;
}
.fha
{
	display:flex;
	justify-content:space-around;
}
.fhb
{
	display:flex;
	justify-content:space-between;
}
.fve
{
	display:flex;
	flex-direction:column;
	justify-content:flex-end;
}
.fvc
{
	display:flex;
	flex-direction:column;
	justify-content:center;
}
.fvs
{
	display:flex;
	flex-direction:column;
	justify-content:flex-start;
}
.fva
{
	display:flex;
	flex-direction:column;
	justify-content:space-around;
}
</style>
